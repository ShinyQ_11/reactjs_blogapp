import React from 'react';
import './Hello.css';
// Statefull Component
class StateFullComponent extends React.Component {
  render(){
    return <p>Hello World, Ini Statefull Component</p>
  }
}

export default StateFullComponent;
