import React from 'react';
import './Blog.css';

// Stateless (Functional Component)
const Blog = (props) => {
  return (
    <div className="blog-wrapper">
      <div className="img-thumb">
          <img src="https://i.ytimg.com/vi/pVp-m5HJvgU/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLBMIdSWd4Tni2BwpbnVZMRbFguR_A" alt=""/>
      </div>

      <p className="judul">{props.judul}</p>
      <p className="deskripsi">{props.deskripsi}</p>
    </div>
  )
}

export default Blog;
