import React from 'react';

// Stateless (Functional Component)
const HelloWorld = () => {
  return <p>Hello World, Ini Stateless Component</p>
}

export default HelloWorld;
