import React, {Component} from 'react';
import Blog from '../../component/blog/Blog';
import Produk from '../../component/container/Produk/Produk';
import LifeCycleComp from '../../component/container/LifeCycleComp/LifeCycleComp';

class Home extends Component {
  render(){
    return(
      <div>
        {/*<p className="utama"> Blog Component </p>
        <Blog judul="Ini Judul 1" deskripsi="ini deskripsi 1"/>
        <Blog judul="Ini Judul 2" deskripsi="ini deskripsi 2"/>
        <Blog judul="Ini Judul 3" deskripsi="ini deskripsi 3"/>
        <Blog judul="Ini Judul 4" deskripsi="ini deskripsi 4"/>
        <Blog judul="Ini Judul 5" deskripsi="ini deskripsi 5"/>
        <Blog judul="Ini Judul 6" deskripsi="ini deskripsi 6"/>
        <Blog judul="Ini Judul 7" deskripsi="ini deskripsi 7"/>
        <Blog judul="Ini Judul 8" deskripsi="ini deskripsi 8"/>
        <Blog judul="Ini Judul 9" deskripsi="ini deskripsi 9"/>
        <Blog judul="Ini Judul 10" deskripsi="ini deskripsi 10" />
        counter
        <hr />
        <Produk /> */}
        <p> Life Cycle Component </p>
        <hr />
        <LifeCycleComp />
      </div>
    )
  }
}

export default Home;
