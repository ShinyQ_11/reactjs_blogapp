import React, {Component,Fragment} from 'react';
import './Produk.css';
import CardProduk from '../CardProduk/CardProduk';

class Produk extends Component{
  state = {
    order : 4,
    name : 'Kurniadi'
  }

  handleCounterChange = (newValue) => {
    this.setState({
      order: newValue
    })
  }

  render(){
    return(
      <Fragment>
        <div className="header">
            <div className="logo">
              <img src="https://etanee.id/img/content/img_logo_etanee_white.svg"/>
            </div>
            <div className="trolley">
              <img src="https://etanee.id/img/icon/ic_cart_white.svg"/>
              <div className="count">{this.state.order}</div>
            </div>
        </div>

        <CardProduk onCounterChange={(value)=> this.handleCounterChange(value)}/>
      </Fragment>
    );
  }
}

export default Produk;
